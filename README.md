# dotfiles-arch
All relevant dotfiles of my Arch Linux System.

## Screenshots
![Clean](screenshots/screenshot_plain.png)
![rofi](screenshots/screenshot_rofi.png)
![Working Setup](screenshots/screenshot_busy.png)
All files for those reports can be found ![here](https://github.com/alexander-schoch/oacp1-protocols). If you want to replicate my TeX design, just look there.
![Music Setup](screenshots/screenshot_music.png)
The Script `music.sh` can be found ![here](https://github.com/aleander-schoch/scripts)
![Fakebusy/System Information](screenshots/screenshot_fakebusy.png)
The Script `series-stream.sh` can be found ![here](https://github.com/aleander-schoch/scripts)
 
## Wallpaper
![Wallpaper](wallpaper.png)
This wallpaper has been created by Dominic Gut.
