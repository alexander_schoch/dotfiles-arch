# If you come from bash you might have to change your $PATH.
export PATH=$PATH:/home/alexander_schoch/.scripts/links/:/home/alexander_schoch/.gem/ruby/2.7.0/bin

# Path to your oh-my-zsh installation.
ZSH=/usr/share/oh-my-zsh/

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="darkblood"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)


# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh

alias open='xdg-open'
#alias poweroff='sudo pacman -Syu --noconfirm && poweroff'

# Open a new window in this term's cwd
alias nw="terminator"
nwZle() { zle push-line; BUFFER="nw"; zle accept-line; }
zle -N nwZle
# CTRL+n
bindkey '^n' nwZle
set -o vi

# Color scheme: pywal
(cat ~/.cache/wal/sequences &)
alias hotspot="sudo iw dev wlp59s0 connect 'Alexander Schoch'" 
alias stick="sudo mount /dev/sdc1 /media/USB"
alias stickumnt="cd ~ && sudo umount /dev/sdc1"
alias dunnet="emacs -batch -l dunnet"
alias kdec="kdeconnect-cli -n NUBIA --share $( pwd )/"

alias removalreason='echo -e "Your submission has been removed.\n\nRule #0 violation.\n\nIf you feel that it has been removed in error, please [message us](https://www.reddit.com/message/compose?to=%2Fr%2FProgrammerHumor) so that we may review it." | xsel -ib'
alias raspi='ssh -Y pi@192.168.1.106 -p 10600'
alias secretAdventure='emacs -batch -l dunnet'
alias thealt='ssh files@alternative.vsos.ethz.ch'
alias thealt_webserver="ssh project2@thealternative.ch"
alias vpn='ethnet && sudo openconnect https://sslvpn.ethz.ch/student-net -u schochal@student-net.ethz.ch'
alias ethnet='keepassxc-cli clip ~/Documents/Private/Passwords.kdbx ETHNET -a Password'
alias nethz='keepassxc-cli clip ~/Documents/Private/Passwords.kdbx nethz -a Password'
alias getip='dig @ns1-1.akamaitech.net whoami.akamai.net +short'

#alias mpvv="mpv --ytdl-raw-options=force-ipv4= --ytdl-format=best --term-playing-msg='now playing: \e[1m\e[4m${media-title}'"
#alias mpva="mpv --ytdl-raw-options=force-ipv4= --ytdl-format=bestaudio --shuffle --term-playing-msg='now playing: \e[1m\e[4m${media-title}'"
alias ytdlv='youtube-dl --format=best -4 -i'
alias ytdla='youtube-dl -x --audio-format=mp3 -4 -i'

alias pscircle_reload='pscircle --output-width=1920 --output-height=1080 --background-image=/home/alexander_schoch/Pictures/Wallpapers/wallpaper_rsz.png --output=/tmp/wallpaper.png --tree-radius-increment=150 --tree-font-face=Mono --dot-radius=3 --link-width=1 --dot-color-min=a4e8f6 --toplists-font-face=Mono'

alias mnt_gentoo='sudo mount /dev/sda6 /mnt && cd /mnt/home/alexander_schoch/'
alias mnt_ubuntu='sudo mount /dev/sda4 /mnt && cd /mnt/home/alexander_schoch/'
eval $(thefuck --alias)

# Weather report. no argument means schongau, 'zh' means zurich, anything else will be taken as is.
function weather()
{
  if [[ -z $1 ]] && curl wttr.in/Schongau_LU && return
  if [[ $1 = 'zh' ]] && curl wttr.in/Zurich || curl wttr.in/$1
}

function search()
{
  sudo find . -iname "*$1*"
}

function set_brightness()
{
  echo $1 > /sys/class/backlight/intel_backlight/brightness
}

function mpva ()
{
  mpv --ytdl-raw-options=force-ipv4= --ytdl-format=bestaudio --shuffle --term-playing-msg='now playing: \e[1m\e[4m${media-title}' $1
}

function mpvv ()
{
mpv --ytdl-raw-options=force-ipv4= --ytdl-format=best --term-playing-msg='now playing: \e[1m\e[4m${media-title}' $1

}

function jbadd ()
{
  echo "$1" >> /home/alexander_schoch/.scripts/jukebox/queue
}

function antrag ()
{
  if [[ -n $1 ]] 
  then
    cp -r ~/.templates/antrag_vseth ./$1
    mv ./$1/antrag.tex ./$1/$1.tex
  fi
}

function infoadd()
{
  src="/home/alexander_schoch/todo/ex5"
  dst="/home/alexander_schoch/Documents/ETHZ/Assistenzen/info1/Uebungen/ex5"
  name=$( echo $1 | sed "s/ /_/g" )
  echo "Creting Folder $dst/$name"
  mkdir -p "$dst/$name"
  echo "Extracting tar archive for $name"
  tar xf "$src/ex5_$name.tar" -C "$dst/$name"
  retval=$?
  [[ $2 != "--skip-errors" ]] && [[ $retval != 0 ]] && echo "--> tar failed" && return
  sed -i 's///g' $dst/$name/*.cc
  sed -i 's/\t/  /g' $dst/$name/*.cc
  indent -bli0 -lc1000 -l1000 --no-tabs -lp $dst/$name/*.cc
  cp $src/Makefile $dst/$name
  git add "$dst/$name"
  git commit -m "add ex5 $1"
}

function infocorrect()
{
  dst="/home/alexander_schoch/Documents/ETHZ/Assistenzen/info1/Uebungen/ex5/"
  name=$( echo $1 | sed "s/ /_/g" )
  git add "$dst/$name"
  git commit -m "correct ex5 $1"
  git push
}

alias aocp2='sudo mount -t cifs "//ames.ethz.ch/AOCP 2 Students" /mnt -o username=aocp2studi'

alias blitz='ssh -4 w3_blitz@blitz.ethz.ch'

alias p='ping gentoo.org'

function otex()
{
  if [[ -n $1 ]]
  then
    file=$( echo $1 | cut -d '.' -f 1 )
  else 
    file=$( ls *.tex | head -n 1 | cut -d '.' -f 1 )
  fi
  zathura $file.pdf &
  vim $file.tex
}

alias euler_sshfs='sshfs schochal@euler.ethz.ch: ~/Data/euler'
alias euler_ssh='ssh -XY schochal@euler.ethz.ch'

function sendtoalex(){
  export DISPLAY=:0
  cd ~/Downloads
  echo $1 > somefile.txt
  sent somefile.txt
}
